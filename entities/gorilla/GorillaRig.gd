extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.

func _on_Fist_body_entered(body):
	get_tree().change_scene("res://HUD/GameOverEnemy.tscn")

func _on_LeftArm_body_entered(body):
	get_tree().change_scene("res://HUD/GameOverEnemy.tscn")

func explode():
	queue_free()

func _on_Head_body_entered(body):
	explode()
	get_tree().change_scene("res://HUD/Win.tscn")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass



