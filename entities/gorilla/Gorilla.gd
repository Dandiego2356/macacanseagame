extends KinematicBody2D

signal health_changed
signal dead
signal hit

export (PackedScene) var ParrotFeather

export (float) var rotation_speed
export (float) var hit_cooldown
export (int) var health
export (float) var cooldown

var can_hit = true
var alive = true
var velocity = Vector2()

func hit():
	if can_hit:
		can_hit = false
		$HitTimer.start()
		var dir = Vector2(1, 0).rotated($Body.global_rotation)

func _ready():
	pass

func control(delta):
	pass

func _physics_process(delta):
	if not alive:
		return
		control(delta)
		move_and_slide(velocity)




func _on_HitTimer_timeout():
	can_hit = true
	#$Cooldown.start()


#func _on_Cooldown_timeout():