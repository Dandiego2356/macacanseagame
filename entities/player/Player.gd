extends KinematicBody2D

class_name Player

var velocity = Vector2();
const UP = Vector2(0, -1);
const SLOPE_STOP = 64
var move_speed = 5 * 90
var gravity = 1200
var jump_velocity = -600
onready var anim = get_node("Body/CharacterRig/AnimationPlayer")

const JUMP_FORCE = 800
const GRAVITY = 2000

var timer = null
var timer_delay = 3


func _ready():
	timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(timer_delay)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)

func _physics_process(delta):
	velocity.y += gravity* delta
	velocity = move_and_slide(velocity, UP, SLOPE_STOP);
	_get_input();
	if position.y > 600:
		get_tree().change_scene("res://HUD/GameOver.tscn")
	if position.x > 7040:
		move_speed = 0;
		jump_velocity = 0



func _input(event):
	if event.is_action_pressed("ui_select") and velocity.y == 0:
		velocity.y = jump_velocity


func _get_input():
	var move_direction = -int(Input.is_action_pressed("ui_left")) + int(Input.is_action_pressed("ui_right"))
	velocity.x = lerp(velocity.x, move_speed * move_direction, 0.2);
	if move_direction != 0:
		$Body.scale.x = move_direction
	if (Input.is_action_pressed("ui_left") and anim.get_current_animation() != "jump") || (Input.is_action_pressed("ui_right") and anim.get_current_animation() != "jump"):
		anim.play("run");
		if velocity.y != 0:
			anim.stop()
			anim.set_current_animation("jump")
	elif velocity.y != 0:
		anim.play("jump");
	elif position.x > 7040:
		anim.set_current_animation("win");
		anim.play("win");
	else:
		anim.play("idle");
	if Input.is_action_just_pressed("ui_select"):
		anim.play("jump");

func on_timeout_complete():
	get_node("AudioStreamPlayer2D").play();


func _on_AudioStreamPlayer2D_finished():
	timer.start();
