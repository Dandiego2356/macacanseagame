extends "res://entities/parrot/Parrot.gd"

export (float) var wing_speed = 5.0
export (int) var detect_radius = 400

onready var anim = get_node("Body/ParrotRig/AnimationPlayer")
var target = null

func _ready():
	var circle = CircleShape2D.new()
	$DetectRadius/CollisionShape2D.shape = circle
	$DetectRadius/CollisionShape2D.shape.radius = detect_radius
	anim.play("fly");

func _process(delta):
	if target:
		var target_dir = (target.global_position - global_position).normalized()
		var current_dir = Vector2(1, 0).rotated($Body.global_rotation)
		$Body.global_rotation = current_dir.linear_interpolate(target_dir, wing_speed * delta).angle()
		if target_dir.dot(current_dir) > 0.9:
			shoot()

func _on_DetectRadius_body_entered(body):
		if body.name == "Player":
			target = body
		


func _on_DetectRadius_body_exited(body):
	if body == target:
		target = null
