extends KinematicBody2D

signal health_changed
signal dead
signal shoot

export (PackedScene) var ParrotFeather
export (int) var speed
export (float) var rotation_speed
export (float) var parrotfeather_cooldown
export (int) var health
export (float) var cooldown

var velocity = Vector2()
var can_shoot = true
var alive = true

func shoot():
	if can_shoot:
		can_shoot = false
		$FeatherTimer.start()
		var dir = Vector2(1, 0).rotated($Body.global_rotation)
		emit_signal('shoot', ParrotFeather, $Wing.global_position, dir)

func _ready():
	$FeatherTimer.wait_time = parrotfeather_cooldown
	$Cooldown.wait_time = cooldown

func control(delta):
	pass

func _physics_process(delta):
	if not alive:
		return
		control(delta)
		move_and_slide(velocity)



func _on_FeatherTimer_timeout():
	can_shoot = true
	#$Cooldown.start()


#func _on_Cooldown_timeout():
	can_shoot = true
