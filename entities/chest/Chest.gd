extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var timer = null
var timer_delay = 7


func _ready():
	get_node("closed").show()
	get_node("opened").hide()
	
	timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(timer_delay)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)



func on_timeout_complete():
	get_tree().change_scene("res://HUD/LevelSelect.tscn")



func _on_Area2D_body_entered(body):
	get_node("closed").hide()
	get_node("opened").show()
	timer.start()
	
