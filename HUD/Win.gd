extends Node


onready var anim = get_node("CharacterRig/AnimationPlayer")

# Called when the node enters the scene tree for the first time.
func _ready():
	anim.play("win");
	Music.get_node("Level3").stop();
	Music.get_node("Level2").play();

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
