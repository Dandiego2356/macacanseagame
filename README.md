# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Storyline:
Two pirate crews are fighting each other in the Macacan Sea, for lost treasure, bananas, and soup. The player’s crews are led by Captain Macondo,
of the Plundering Primates. and the enemy by Captain Quickbeard, of the Mutiny Monkeys. In the opening scene, Captain Macondo is defeated and an explosion 
sends him flying off the ship. He lands on a deserted island, where he has to beat wild animals, like parrots and crocodiles. His goal is to reach the other side,
where his ship will hopefully be waiting for him. Once he reaches the end of the island, his ship is there and they set sail once again to get the lost treasure
they had originally been searching for. They arrive at another island, and when they get to where the treasure would be, they are ambushed the Mutiny Monkeys. 
Captain Macondo and Captain Quickbeard have a boss fight. Captain Quickbeard is a giant gorilla with incredible strength. He throws trees at Macondo and charges 
him. When Captain Macondo kills Captain Quickbeard, the game ends, and the Plundering Primates find the lost treasure. If the game is completed, we will try to 
add other game modes, such as a turn-based naval warfare and a survival mode, where Captain Macondo needs to survive waves of enemies and fights with Quickbeard.

We have been doing all of the sprites and we have found some assets that could help as the terrain and platforms for the level. We need to work on the player, 
the memory system (so that the game saves the player’s progress), the enemies, and the UI.

Mechanics:
Each ship will have a variety of different power-ups, ranging from simple cannonballs to Megalodon Bombs, Kraken bombs, monkeys bombs…
They are fighting in the open sea, so the terrain is constantly changing, so players have to calculate and time the shots of the different cannon balls.
The game will be monetized through microtransactions and ads.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact