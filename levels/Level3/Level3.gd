extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _on_ParrotEnemy_shoot(parrotfeather, _position, _direction):
    var f = parrotfeather.instance()
    add_child(f)
    f.start(_position, _direction)

func _ready():
	Music.get_node("Level2").stop();
	Music.get_node("Level3").play();
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

